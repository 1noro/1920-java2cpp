
#include "genFunc.hpp"

#include <vector>       //usar arrays
#include <string>       //usar strings
#include <sstream>      //para hacer el split

using namespace std;    //para no tener que poner std:: delante de cada cosa

vector<string> split(const string& s, char delimiter) {
    vector<string> tokens;
    string token;
    istringstream tokenStream(s);
    while (getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}

bool isNumber(string str) {
    int i = 0;
    bool out = true;
	for (i = 0; i < str.length(); i++) {
        if (((int) str[i] - (int) '0') >= 10) out = false;
	}
    // cout << "hey\n";
    return out;
}
