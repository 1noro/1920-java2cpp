
#include <iostream>     //imprimir por pantalla
#include <fstream>      //leer archivos
#include <string>       //usar strings
#include <vector>       //usar arrays
#include <sstream>      //para hacer el split
#include <typeinfo>     //para saber la informacion de los tipos

using namespace std;    //para no tener que poner std:: delante de cada cosa

// --- VARIABLES GLOBALES ------------------------------------------------------
string finAltas = "fin";

// --- UTILIDADES --------------------------------------------------------------
vector<string> split(const string& s, char delimiter) {
    vector<string> tokens;
    string token;
    istringstream tokenStream(s);
    while (getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}

bool isNumber(string str) {
    int i = 0;
    bool out = true;
	for (i = 0; i < str.length(); i++) {
        if (((int) str[i] - (int) '0') >= 10) out = false;
	}
    return out;
}

// --- INTERFAZ DE USUARIO -----------------------------------------------------
void printMenu() {
    cout << "\n";
    cout << "--- MENU ---\n";
    cout << " 1 - Altas\n";
    cout << " 2 - Listados\n";
    cout << " 3 - Media de edad\n";
    cout << " 4 - Nombre del mayor y menor (no rep.)\n";
    cout << "\n";
    cout << " 0 - Salir\n";
    cout << "\n";
}

int readOpt(int max) {
	int out = 0;
	do {
		cout << "Escribe una opción: ";
		cin >> out;
	} while (out < 0 || out > max);
    cout << "\n";
	return out;
}

// --- LECTURAS Y VALIDACIONES DE FORMATO --------------------------------------
string readNombre() {
    string out = "";
    do {
        cout << " Nombre (15 char max); '" << finAltas << "' para acabar: ";
        cin >> out;
    } while (out != finAltas && out.length() > 15);
    return out;
}

int readEdad() {
    string edad_str = "";
	int edad = 0;
	bool continuar = true;

	do {
		cout << " Edad: ";
		cin >> edad_str;
		if (isNumber(edad_str)) {
            edad = stoi(edad_str);
            continuar = false;
        }
	} while(continuar);

	return edad;
}

// --- FUNCIONES GENÉRICAS -----------------------------------------------------
void saveAlta(string nombre, int edad, string fullDir) {
    ofstream f;
    f.open(fullDir, ios::app); //ios::app --> append to file
    f << nombre << "#" << edad << "\n";
    f.close();
}

void altas(string fullDir) {
    //Alta myAlta;
	string nombre = "";
	int edad = 0;
	do {
		cout << ">> Nueva alta:\n";
		nombre = readNombre();
		if (nombre != finAltas) {
			edad = readEdad();
			// myAlta = new Alta(nombre, edad);
			saveAlta(nombre, edad, fullDir);
            cout << " #GUARDADO:" << nombre << "#" << edad << "\n";
		}
	} while(nombre != finAltas);
}

void printListados(string fullDir) {
    ifstream f;
    string line = "";
    vector<string> part;

    cout << "--- LISTADOS ---\n";
    f.open(fullDir);
    if (!f) {
        cerr << "[FAIL] Unable to open file '" << fullDir << "'\n";
        exit(1); // call system to stop
    }
    while (f >> line) {
        // cout << "line: '" << line << "'\n";
        part = split(line, '#');
        cout << part.at(0) << "\t\t" << part.at(1) << "\n";
    }
    f.close();
}

void printMediaEdad(string fullDir) {
    ifstream f;
    string line = "";
    vector<string> part;
    int cont = 0;
    int suma = 0;

    cout << "--- MEDIA ---\n";
    f.open(fullDir);
    if (!f) {
        cerr << "[FAIL] Unable to open file '" << fullDir << "'\n";
        exit(1); // call system to stop
    }
    while (f >> line) {
        part = split(line, '#');
        suma += stoi(part.at(1));
        cont++;
    }
    f.close();
    cout << "suma: " << suma << '\n';
    cout << "cont: " << cont << '\n';
    cout << "avg:  " << ((float) suma / (float) cont) << '\n';
}

void printNombreMayorMenor(string fullDir) {
    ifstream f;
    string line = "";
    vector<string> part;
    int eMax = 0;
    int eMin = 999;
    string nMax = "";
    string nMin = "";

    cout << "--- NOMBRES MAX Y MIN ---\n";
    f.open(fullDir);
    if (!f) {
        cerr << "[FAIL] Unable to open file '" << fullDir << "'\n";
        exit(1); // call system to stop
    }
    while (f >> line) {
        part = split(line, '#');
        if (stoi(part.at(1)) > eMax) {
            nMax = part.at(0);
            eMax = stoi(part.at(1));
        }
        if (stoi(part.at(1)) < eMin) {
            nMin = part.at(0);
            eMin = stoi(part.at(1));
        }
    }
    f.close();
    cout << "Mayor: " << nMax << " (" << eMax << ")" << '\n';
    cout << "Menor: " << nMin << " (" << eMin << ")" << '\n';
}

// --- MAIN --------------------------------------------------------------------
int main(int argc, char** argv) {
    // string fullDir = "/opt/archivosJava/altasEdad01cpp.dat";
    string fullDir = "";
    if (argv[1] == NULL) fullDir = "aa.dat";
        else fullDir = argv[1];

    int opt = 0;
    do {
        printMenu();
        opt = readOpt(4);
        if (opt != 0) {
            if (opt == 1) altas(fullDir);
            else if (opt == 2) printListados(fullDir);
            else if (opt == 3) printMediaEdad(fullDir);
            else if (opt == 4) printNombreMayorMenor(fullDir);
        }
    } while (opt != 0);
    cout << "Bye (;︵;)\n";
    return 0;
}

// --- CODE UTILS --------------------------------------------------------------
// for (auto it = part.begin(); it != part.end(); it++)
//     cout << *it << "\n";
