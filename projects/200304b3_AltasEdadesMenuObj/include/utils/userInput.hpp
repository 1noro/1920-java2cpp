
#include <iostream>     //imprimir por pantalla
#include <string>       //usar strings

using namespace std;    //para no tener que poner std:: delante de cada cosa

// --- INTERFAZ DE USUARIO -----------------------------------------------------
int readOpt(int max);

// --- LECTURAS Y VALIDACIONES DE FORMATO --------------------------------------
string readNombre(string finAltas);
int readEdad();
