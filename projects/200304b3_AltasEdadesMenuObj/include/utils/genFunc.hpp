
#include <vector>       //usar arrays
#include <string>       //usar strings

using namespace std;    //para no tener que poner std:: delante de cada cosa

vector<string> split(const string& s, char delimiter);
bool isNumber(string str);
string autotab2(string str);
